---
layout: markdown_page
title: "All-Remote Pick Your Brain"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

On this page, we're detailing how the public can request a Pick Your Brain interview focused on all-remote. 

## Why we offer public interviews on all-remote

GitLab believes that [everyone can contribute](/company/strategy/#mission). We also believe that all-remote organizations can fundamentally change the world by offering a different way to work. From [reversing](/company/culture/all-remote/benefits/) rural depopulation to providing [opportunity](/company/culture/all-remote/people/) to a more diverse set of people, we want all-remote to become less the exception, and more the [norm](/company/culture/all-remote/jobs/). 

There is no formal guidebook on building an all-remote company, and what works for a new startup may not map precisely to a larger organization. 

While we are [proponents](/company/culture/all-remote/benefits/) of all-remote, we recognize that there are [drawbacks](/company/culture/all-remote/drawbacks/) as well as individuals who are skeptical about its feasability. We embrace detractors and those who are inquisitive about making all-remote work. We're interested in hearing about challenges faced by others implementing remote work, so we can ideally find and [document](/handbook/documentation/) solutions.

## Scheduling a Pick Your Brain interview on all-remote

Requests for interviews concerning advice on all-remote may be initiated by completing and submitting [this Google Form](https://docs.google.com/forms/d/1TNdIIDYRJJGzTlbEN2kI_ok6XV9ieHqk2CtnGla4clw/edit). In the request, please include a brief overview of topics you hope to discuss, along with key questions you're prepared to ask.

These 20 to 30 minute interviews with [GitLab's all-remote culture curator](https://gitlab.com/dmurph) will be recorded though Zoom and shared on the [GitLab Unfiltered](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A) YouTube channel. 

We invite guests to surface challenges and examples as points of discussion. Post-interview, and where applicable, we will iterate on relevant handbook pages to document questions and solutions.

GitLab is a very [transparent](/handbook/values/#transparency) company and many things that are normally confidential can be found in our [handbook](/handbook/), available online. Please consider looking at the following pages prior to the meeting. 

1. [GitLab's All-Remote Culture](/company/culture/all-remote/)
	1. [Advantages](/company/culture/all-remote/benefits/)
	1. [Disadvantages](/company/culture/all-remote/drawbacks/)
	1. [How to evaluate a remote job](/company/culture/all-remote/evaluate/)
	1. [Scaling a remote company](/company/culture/all-remote/scaling/)
	1. [People](/company/culture/all-remote/people/)
	1. [Jobs](/company/culture/all-remote/jobs/)
	1. [Management](/company/culture/all-remote/management/)
	1. [Hiring](/company/culture/all-remote/hiring/)
	1. [Compensation](/company/culture/all-remote/compensation/)
	1. [Learning and Development](/company/culture/all-remote/learning-and-development/)
	1. [Informal Communication](/company/culture/all-remote/informal-communication/)
	1. [Meetings](/company/culture/all-remote/meetings/)
	1. [Stories](/company/culture/all-remote/stories/)
	1. [Stages of remote work](/company/culture/all-remote/stages/)
	1. [Hybrid-remote](/company/culture/all-remote/hybrid-remote/)
	1. [How it works at GitLab](/company/culture/all-remote/tips/)
1. [History](/company/history/)
1. [About](/company/)

Before an interview is scheduled, guests will be asked to send over an agenda with proposed questions and topics. 

----
Return to the main [all-remote page](/company/culture/all-remote/).
