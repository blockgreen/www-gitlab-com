---
layout: handbook-page-toc
title: "Safeguard"
---

Can't find what you're looking for? Try the main [People Operations page](/handbook/people-group/).

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

----

The following benefits are provided by [Safeguard](https://www.safeguardglobal.com/) and apply to team members who are contracted through Safeguard. If there are any questions, these should be directed to People Operations at GitLab who will then contact the appropriate individual at Safeguard.

## Ireland

- Currently Safeguard does not provide private healthcare
  * If GitLab did want to look into implementing private healthcare with Safeguard, the costs would be plan dependent and based on level of cover and workers age.
    - Providers ask for application forms to be completed by workers in the first instance in order to obtain individual quotes. Providers will not quote without completed application forms.
  * GitLab will continue to review implementing Medical cover in Ireland.
- Safeguard does provide a pension via Zurich, if individuals would like to join this scheme, a leaflet can be found by clicking on this [link](https://drive.google.com/file/d/1GRasMwjchtKSw4ZkPJNJsiCjAU-MOsNH/view?usp=sharing). Please note that at this time there are no employer contributions.
  * Any changes to the scheme would need to be documented via Safeguard through a contract variation.
- Safeguard does not offer Tax Saver & Bike to Work benefits.
  * As these schemes are salary sacrifice Safeguard would need to know in advance of any set up what the level of take-up would be to see if set up was viable.
  * GitLab will continue to review offering the Tax Saver & Bike to Work benefits.
- Safeguard does not offer Life Assurance.
  * Safeguard would need to generate a plan specific to GitLab. GitLab and Safeguard will continue to review securing a separate policy for Gitlab.

## Spain

_We are currently unable to hire any more employees or contractors in Spain. Please see [Country Hiring Guidelines](/jobs/faq/#country-hiring-guidelines) for more information._

- Currently Safeguard does not provide private healthcare
- Accruals for 13th and 14th month salaries
- General risks and unemployment insurance
- Salary guarantee fund (FOGASA)
- Work accident insurance

 GitLab currently has no plans to offer Life Insurance, Private Medical cover, or a private pension due to the government cover in Spain. If GitLab did want to look into offering such benefits through Safeguard, the employees would need to subscribe themselves to the insurance companies and they would be paid for it in the form of allowance on a monthly basis covering the price that the insurance company requests; this would then be billed to Gitlab.

## Hungary

- Healthcare insurance according to Hungarian laws
- Pension according to Hungarian laws

> You may receive your payslip from this email address: abacus@whc.hu on your personal email ( the one used for correspondence with Safeguard). The payslip is going to contain an encrypted PDF.


## Other countries

Safeguard uses multiple third parties across the globe to assist in locations where they do not have a direct payroll.
