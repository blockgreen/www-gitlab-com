---
layout: handbook-page-toc
title: "Recruiting Process - Recruiter Tasks"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Recruiting Process Framework - Recruiter Tasks
{: #framework-recr}

### Step 2/R: Create vacancy in Greenhouse

Create the vacancy in Greenhouse following the [instructions in the handbook](/handbook/hiring/vacancies/#open-the-vacancy-in-greenhouse) or by watching the [internal training video](https://drive.google.com/file/d/1S2d8XE6Ri6U4fAz6jh3R9DC593YhTcjF/view?usp=sharing).

### Step 3/R: Post vacancy, check details

Follow the steps outlined in the handbook to [complete and open a vacancy in Greenhouse](/handbook/hiring/vacancies/#recruiting-team-tasks). Ensure that the role is assigned to the correct coordinator, that the hiring team and interview plan are correct, that appropriate access is given, that the correct notifications are set up, and that the job description is populated and has a LinkedIn job wrapping code. Publish the vacancy on the GitLab jobs page and add it to the team page.

### Step 4/R: Identify sourcing needs

The Recruiter will determine if a sourcer is needed for the vacancy. If so, they will reach out to the sourcer aligned to the team/location of the vacancy to request sourcing efforts.

### Step 5/R: Schedule intake

It’s been said that the most important meeting in the recruiting process is not between the recruiter and candidate but rather the discussion that takes place between the hiring manager and recruiter before the search is kicked off. The recruiter is an extension of the hiring manager in the process. The intake session provides an opportunity to gain alignment on the role, job requirements, search criteria, and, perhaps most importantly, the expectations and responsibilities between the recruiter and hiring manager in the process.

The recruiter should come prepared to the intake session with insights to serve as a talent advisor vs. order taker. For example:
- Specific markets or companies to target
- Adjustments to the vacancy description to make sure we are targeting the ideal candidate and that the vacancy description is engaging
- Estimated SLAs based on similar roles filled

It is recommended to review the vacancy description in advance of intake so you can focus most of your time on strategic questions and aligning on the interview process, SLAs, and next steps.

**Template Resource:**

> Hello [Hiring Manager],
>
> I’m excited to be able to partner with you on your [TITLE] role. You will see an intake session scheduled on your calendar soon. Before we connect it is important to start thinking about a few things:
>
> Who will your interview team consist of? Your first round interviews will consist of 2-4 members from your team, in individual consecutive interviews or in a panel interview set up or even both. Final interviews will include 1-2 leadership or the C-level executive from your division.
> Are there any necessary assessments included in your interview process? Assessments should be utilized only when needed to eliminate an applicant, based around specific skill sets required for the role.
> Is there any upcoming travel or vacations within the team that could prevent delays on the hiring process?
>
> If you’d like to update any details on your vacancy description prior to our call please do so by making a merge request on the job family description on GitLab.com.

### Step 6/R: Complete intake

**Learn the job:**

- Tell me about this role, what are some of the day to day responsibilities for someone in this role?
- What does your overall team look like, who will this individual be reporting into? Who will this individual be working closely with? Where are the gaps in your team's experience that this person could help with?
- What are some of the specific qualifications or skill sets you are looking for?
- What are some of the key competencies you are looking for in a candidate?
- Are there any companies that you can recommend where we may be able to source top talent from?
- What problems are you aiming to solve by hiring for this role?
- What makes this role appealing? How would you sell it to candidates?
- What are three key questions I should be asking candidates in the screening call?
- Locations to target/exclude - budget/need for Bay Area/other high cost markets?
- What separates a good candidate versus a great candidate for this role?

**Complete the following:**

- Align on process, utilizing resources found in [Recruiting Process Framework](/handbook/hiring/recruiting-framework).
- Work with hiring manager to update the internal [hiring processes repository](https://gitlab.com/gitlab-com/people-ops/hiring-processes) to reflect interview teams and plan.
- Work with hiring manager to ensure calendars are up to date for the interview team. Are there any upcoming known vacations or travel?
- Work with hiring manager to ensure ["working hours" in Google Calendar](https://support.google.com/calendar/answer/7638168?hl=en) are inputted for the hiring manager and interview team.
- Align on who will be included in first-round interviews (1 interviewer), second-round interviews (2-3 interviewers), and final-round interviews (1-2 exec or c-level interviewers) and length of each interview.

If a private Slack channel is setup for easy communication between the hiring team use the Slack recruiting channel naming convention: recruiting-________

### Step 7/R: Find top talent, schedule screens

[Review resumes](/handbook/hiring/greenhouse/#application-review) of applicants who have applied to the role directly, as well as any referrals or internal applicants, and appropriately take action by either declining or advancing.

For those who are recognized as top talent, reach out to them for availability using templates in Greenhouse. Once the candidate has responded, [schedule a screening call](/handbook/hiring/greenhouse/#scheduling-interviews-with-greenhouse) and send a confirmation to the applicant.

Source passive applicants through job boards, such as LinkedIn or Indeed. If a passive applicant is responsive, import applicant into Greenhouse and schedule a screening call with them through Greenhouse.

### Step 9/R: Send assessment, if applicable

As determined previously in the intake session with the hiring manager, if there is an assessment involved in the initial interview process, send candidates the assessment through the provided Greenhouse template. The assessment grader is to be added to the appropriate location on the template email to ensure a notification will be sent to them once the applicant has completed the assessment.

### Step 10/R: Schedule top talent with hiring manager

Once a screening call is completed and a candidate is still determined as top talent, a first-round interview with the hiring manager will be scheduled. To do this, the recruiter will request availability from the applicant using the Greenhouse templates while cc’ing the coordinator, who will action scheduling the next steps.

### Step 13/R: Team interviews

Confirm focus areas that have been assigned to the interview team. Reach out to the candidate to get additional availability, using the Greenhouse templates while cc’ing the coordinator, who will action scheduling the next steps.

### Step 16/R: Host interview team debrief if applicable

An interview debrief can be used following second-round interviews if a need is recognized. This may be determined with the level of the role, the hiring manager's preference, or delayed feedback inputted into Greenhouse causing delays in the process. It is also helpful to evaluate how the process has been going thus far and if we need to change our strategy or approach.

### Step 17/R: Connect with HM on next steps

Follow up with the hiring manager on next steps. Depending on the level of the role and outcome of interviews, next steps may include additional interviews including an executive interview or moving to reference checks.

### Step 18/R: Request references

Once a hiring manager has determined they want to move forward with a candidate, the recruiter will reach out to the candidate to request their references contact details, using the Greenhouse email template.

### Step 21/R: Submit offer

Once references are completed or as they are in progress, the recruiter will move the candidate to offer stage and collect and submit offer details in Greenhouse. The recruiter will ensure offer is routed for approval.

### Step 22/R: Verbalize offer

Once the offer is approved in Greenhouse and references are completed, an offer will be verbalized to the candidate by the recruiter or hiring manager.
