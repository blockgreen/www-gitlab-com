---
layout: handbook-page-toc
title: "Recruiting Process - Hiring Manager Tasks"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Recruiting Process Framework - Hiring Manager Tasks
{: #framework-hm}

### Step 1/HM: Identifying hiring need

1. Determine the purpose of this role
1. Is it a new or existing role?
1. Create or adjust a job description that outlines the positions main responsibilities and basic qualifications
1. See the [vacancy creation process](/handbook/hiring/vacancies/#vacancy-creation-process) in the handbook.

### Step 12/HM: Complete feedback in Greenhouse/next steps

Once the first interview(s) are completed, the interviewer will submit feedback via the designated scorecard in Greenhouse. The hiring manager will have designated areas of focus for each member of the interview team. These will be defined during the intake call and setup in GreeHouse by the recruiter or the CES. 

As a best practice the Hiring Manger should reach out to each individual on the interview plan.

**Example Reachout**

> Thank you for making yourself available to be part of the interview team to fill the [TITLE]. A few things you need to know before interviewing candidates.
>
> Key things we are looking for specific to the role:
> > a. Example 1
> >
> > b. Example 2
> >
> > c. Example 3
>
> Individual focus areas during your interview (attached reference if needed):
> > a. [insert name of Interviewer 1] - [focus area]
> >
> > b. [insert name of Interviewer 2] - [focus area]
> >
> > c. [insert name of Interviewer 3] - [focus area]
>
> **Please** complete your interview notes in Greenhouse within 1 business day of your interview. If you have any trouble please contact your recruiter or CES.
>
> Additionally, your referrals are important to us! If you have any referrals for this role (or other roles that are currently posted at GitLab) please submit them following the [guidelines in the handbook](/handbook/hiring/greenhouse/#making-a-referral).

**Interview Resource**

You can review [sample interview questions for the interview team](https://docs.google.com/document/d/1Eb7GUUH0b9wzf1WxuyKUYhpjP774VbO5sQWtiDZX-PM/edit) based around specific focus areas.

### Step 15/HM: Hiring team to complete feedback in Greenhouse

Once each step in the interview plan is completed, the interview team will submit feedback via the designated scorecard in Greenhouse within 1 business day of their interviews.

### Step 19/HM: Complete references

Once the recruiter/CES has notified the hiring manager of the references sent by the candidate, the hiring manager will complete references to establish the candidate's strengths and opportunities, to set them up for success once joining GitLab. At least 2 references must be completed, and one of them must be a manager or supervisor. 

#### Why Should Hiring Managers Check References

We recorded a training on this subject here:

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/8pdf_rRihcE" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

In short, hiring managers check references:
 - To ensure we are hiring the right candidate.
 - To understand under what circumstances this candidate thrives vs. what circumstances frustrates them.
    - As the hiring manager, you will be the closest to the GitLab team-member and benefit most from learning about them.
 - To build your network.
    - As a hiring manager, you need to build a network for great talent.  Each reference you talk to can be a part of that network. 
 - To use the opportunity to spread the GitLab story.  We could spark new customers and new GitLab team-members.

All GitLab hiring managers should be making the best effort to complete and conduct the reference checks for their candidate.

#### Reference Check Questions
When a candidate passes the initial screening and first rounds of interviews, but before they advance to meet with senior leadership, the recruiting team will reach out to the candidate to collect their references' details. The candidate will be asked to provide three references, at least one of which should be a former or current manager. At a minimum, two references should be completed. Once the recruiting team receives the references' details, they will inform the hiring manager, who should reach out and contact the references provided. If it is not possible to schedule a call via phone, a Hangouts or Zoom meeting can also be arranged if it's convenient. If that is not possible, an email can be sent with the following questions:

- Can you briefly describe your working relationship with them?
- What are they like to work with?
- What can they improve upon?
- What advice would you give their next hiring manager?
- Would you rehire this person?
- If we hired this person, would that make you more likely to work at GitLab, even if you don't know what we do?

You can also elaborate further and ask follow up questions if the opportunity arises. The hiring team will be adding engineering questions into Greenhouse so that all engineering hiring managers have access to the same ones. Additionally, the hiring team will work with each function to identify any other specific questions hiring managers would like to add to Greenhouse for their team.

You should not ask any questions about the person's race, gender, sexual preference, disabilities or health, political affiliations, religion, or family (children). Example questions not to ask:

- Who watches their children while they are at work?
- What types of groups does the candidate belong to that are not work related?

#### Reference Check Review
All reference check feedback should be entered into Greenhouse using the Reference Checks scorecard. To add this information, go to the candidate's profile, make sure they are in the "Reference Checks" stage, and click "Collect Feedback".

It is the hiring manager's responsibility to do the reference checks, but the hiring team may also provide assistance and guidance. You can also refer to [these guidelines](http://www.bothsidesofthetable.com/2014/04/06/how-to-make-better-reference-calls/).

#### If References Don't Provide Full References
Increasingly, organizations have a company policy that prevents their employees from providing references; instead, they are only able to verify employment, including dates of employment and title. Do not judge a candidate because his or her former employer has this policy: it does not mean the candidate was not successful. Instead, go back to the candidate to get the name and contact information for an alternative reference.

#### Internal References
The recruiting team may also ask candidates if they've worked with anyone in the past who currently works at GitLab or knows someone at GitLab that we could talk to.

If a GitLab team-member provides a positive or negative feedback on a candidate, they should provide specific details of their experience and relationship with the candidate to enable the hiring manager to make an informed decision.

#### Backchannel References
As a hiring manager, if you decide to inquire about a candidate through known associates at their current or former employer there are some important steps to ensure
you take.

1. Inform the candidates you are about to start backchannel references and respect their wishes if they request you not
1. Ask the candidates if there are any specific individuals or organizations that should not be contacted as part of the backchannel reference process
