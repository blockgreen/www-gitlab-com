---
layout: handbook-page-toc
title: "BC.1.02 - Business Continuity Plan: Roles and Responsibilities Control Guidance"
---
 
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}
 
# BC.1.02 - Business Continuity Plan: Roles and Responsibilities
 
## Control Statement
Business contingency roles and responsibilities are assigned to individuals and their contact information is communicated to authorized personnel.
 
## Context
Establishing defined roles and responsibilities reduces organization confusion in the event of disruption. Knowing who the [DRI](/handbook/people-group/directly-responsible-individuals/) are and how to contact them empowers faster communication, reduced response times, makes for easier and more substantive triage, and ultimately, more speedy recovery from disruption. 
 
## Scope
Roles and Responsibilities should be defined for the following environments and systems:
* BC plan for gitlab.com
* BC plan for customers.gitlab.com (Azure)
* BC plan for license.gitlab.com (AWS)
* Processes and procedures that support business operations and above environments


## Ownership
* Business Operations owns this control.

 
## Guidance
As part of the esablishment of a BC plan: 
* Document the functions, roles, procedures and key personnel, who should lead the effort and who are all the key players involved.
* Prepare a detailed communication plan with contact information of all key personnel

 
## Additional control information and project tracking
Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Business Continuity Plan: Roles and Responsibilities issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/775). 
 
### Policy Reference
 
## Framework Mapping
* ISO
  * A.17.1.1
  * A.17.1.2
* SOC2 CC
  * CC7.5
  * CC9.1
* SOC2 Availability
  * A1.2
* PCI
  * 12.10.1
 