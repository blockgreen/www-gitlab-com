---
layout: handbook-page-toc
title: "SO.2.02 - De-provisioning Physical Access Control Guidance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# SO.2.02 - De-provisioning Physical Access

## Control Statement

Physical access that is no longer required in the event of a termination or role change is revoked. If applicable, temporary badges are returned prior to exiting facility.

## Context

This control refers to physical access to GitLab datacenters or facilities.

## Scope

This control is not applicable since GitLab has no datacenters or facilities.

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [De-provisioning Physical Access control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/893).

### Policy Reference

## Framework Mapping

* ISO
  * A.11.1.2
* SOC2 CC
  * CC6.4
* PCI
  * 9.2
  * 9.3
  * 9.4.3
  * 9.5
