---
layout: handbook-page-toc
title: "BU.1.03 - Backup Management: Alternate Site Control Guidance"
---
 
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}
 
# BU.1.03 - Backup Management: Alternate Storage
 
## Control Statement
GitLab backups are securely stored in an alternate location from source data.
 
## Context
Backup copies of information, software and system images need to be stored in an alternate site in the event of a disruption to the primary storage location. This supports system availabilty and redundancy.
 
## Scope
Alternate storage controls should cover:
* gitlab.com
* customers.gitlab.com (Azure)
* license.gitlab.com (AWS)


## Ownership
* Control owner:
  * Infrastructure
* Process owner:
  * Infrastructure
 
## Guidance

 
## Additional control information and project tracking
Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Backup Management: Alternate Storage issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/780) . 
 
### Policy Reference
 
## Framework Mapping
* ISO
  * A.12.3.1
* SOC2 CC

* SOC2 Availability

* PCI

 