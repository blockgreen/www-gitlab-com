---
layout: handbook-page-toc
title: "Engineering Management"
---

## How Engineering Management Works at GitLab

At GitLab, we promote two paths for leadership in Engineering. While there is a
healthy degree of overlap between these two ideas, it is helpful and efficient
for us to specialize training and responsibility for each of:

- **Technical leadership**, as represented by [Staff and higher-level
  backend engineers](/job-families/engineering/backend-engineer/#staff-backend-engineer).
- **Professional leadership**, as represented by [Engineering
  management](/job-families/engineering/backend-engineer/#engineering-manager).

While technical leadership tends to come naturally to software engineers,
professional leadership can be more difficult to master. This page will serve as
a training resource and operational guide for current and future managers.

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## General leadership principles

All Engineering Managers should follow the [general leadership
principles](/handbook/leadership/) set out in the handbook. In particular, it is
not uncommon for Engineering Managers to struggle with one or more of the
following areas, so we recommend you review them carefully and discuss your
confidence with your manager:

- [1-1s](/handbook/leadership/1-1/)
- [Providing regular feedback](/handbook/leadership/#giving-performance-feedback)
- [Dealing with underperformance](/handbook/underperformance/)

## Hiring

- **Hiring is a priority.** GitLab is a fast-growing company, and paying
  attention to hiring is one of the highest-leverage activities a manager can
  perform. As long as there are vacancies on your team, hiring should be your top
  priority.
- **Hiring is your responsibility.** You may rely on other members of your team
  to help you evaluate potential candidates. We also have many people at GitLab,
  most notably in Recruiting, who will help you through the process. But it is
  always your responsibility to make timely, high-quality hires - speak up if
  you feel like anything is preventing you from accomplishing this goal.
- **Hiring is about making the team better.** It is easy to try to find
  candidates who can merely perform the functions of the role, and more
  difficult to find people who make the team better while they do it.
- **Hiring is hard.** It often involves making difficult decisions, and learning
  to do it well can take years. In times of rapid growth, it may consume 50% or
  more of your time. Please use every resource available to you, from
  more experienced hiring managers in the company to the many resources
  [available in our handbook](/handbook/hiring/interviewing/).

## Technical Credibility

We expect all managers at GitLab to be technically credible with their teams.
Fluency in our core technologies and architectures is essential because it
enables managers to participate effectively on technical conversations. In order
to maintain this fluency, we encourage managers to participate in coding-related
work to an extent. However, please keep the following advice in mind:

- Avoid critical path work. If work has been scheduled for a release or is
  otherwise blocking other members of the team, it's best left to a developer
  who can focus on it more holistically. As a manager you should expect regular
  interruptions to your day that will make you less effective on this kind of
  task.
- Focus on where you can add the most value. As mentioned above, you won't add
  value on critical path work, but that doesn't mean you can't add value in
  other ways. For a great discussion on this, see this article on [How (and why)
  Should Managers Code](https://medium.com/@johnbarton/how-and-why-should-managers-code-323751799664).
- Plan to review more than you write. Following from all of this, most of your
  "coding-related work" should be in code review - you add more value reviewing
  code to stay up to date with your team and provide them feedback and guidance.
  If you find yourself spending more time writing code than you do reviewing it,
  this may be an indicator that you need to revisit your priorities as a leader.

## Career Coaching for Developers

Outside of [hiring](#hiring), the best way to improve the strengths of your team
is to practice career development coaching with your developers. While they will
not all become Staff Developers or Engineering Managers at GitLab, identifying
their career goals and proactively working towards them is the most effective
way to help everyone improve. In addition to the company-wide notes on [career
mapping and
development](/handbook/people-group/learning-and-development/#career-mapping-and-development),
here are some important considerations for the Engineering team:

- **Technical or Professional Leadership?** This is the first section on our
  [career development
  page](/handbook/engineering/career-development/#individual-contribution-vs-management)
  for a reason. For Senior Developers, this is the most important question to
  ask about their career goals, as it will determine where they go next. For
  anyone who hasn't yet made Senior, this is less important - they need to
  make Senior first - but it's always a good idea to start talking about this
  fork in our career paths.
- **Assess together.** Once you know where your Developer wants to go with their
  careers, it's time to assess their strengths and weaknesses for the position.
  This is best done by reviewing the [responsibilities and
  requirements](/job-families/engineering/backend-engineer/) of the job they're
  interested in. If you both assess it separately, you can compare and contrast
  your results.
- **Create a behavior-based plan.** Once you understand the Developer's
  strengths and weaknesses, help them understand how they can improve by
  focusing on behaviors they can change. Be careful to avoid tasks like "run
  this meeting" or "complete this project," and be careful to avoid personality
  traits like "be less angry" or "be more assertive." "When you realize you're
  in an argument, take a breath and figure out how to move forward
  constructively" is an example of a good behavioral action.
- **Delegate and give feedback.** When you are in agreement about the behavioral
  action plan, start looking for opportunities to delegate appropriate tasks to
  the Developer. A Senior looking to move to Staff may benefit from taking some
  architectural conversations off of your plate, while one looking for a Manager
  job will learn a lot from tackling some project management initiatives.
- **Keep the plan updated.** It's important to remember that this is a coaching
  plan, _not_ a promotion plan. There is never any guarantee that the plan you
  created will result in a promotion if the Developer "checks all the boxes,"
  and you should feel free to add to or remove from the plan as they grow and
  you learn more about them. If and when you feel that they are ready for
  promotion, please follow the [normal promotion
  process](/handbook/people-group/promotions-transfers/).

## Project Management

[Product](/handbook/product/) is responsible for guiding the direction of our
product, and [technical leaders](#how-engineering-management-works-at-gitlab)
are responsible for guiding the technical architecture to meet those
requirements. While Engineering Managers can and should participate in both of
those conversations, their primary responsibility lies in project management:
making sure their team is best positioned to meet both goals as effectively and
efficiently as possible.

- **Capacity tracking/planning** - while heavy methods of estimation don't fit
  well with our approach to work, it's your responsibility to keep an eye on the
  capacity of your team. This allows you to predict capacity, which helps with
  product planning, and also detect problems earlier if an important schedule is
  likely to slip.
- **Collaboration with stakeholders** - in addition to Product Managers and
  members of your team, you are the primary point of contact for any other
  stakeholders for the work your team is doing. You should expect to spend time
  communicating clearly and thoroughly about the state of your ongoing projects.
- **Be aware of your team's status.** Change can happen quickly - the more you
  know about what your team is working on and how it's going, the better you'll
  be able to make the best decisions.
- **Balance engineering initiatives** - make time for [engineering-led
  initiatives](/handbook/engineering/#engineering-led-initiatives) balanced
  against product's needs for your team. 

## Team retrospectives

In addition to the [public function-wide
retrospective](/handbook/engineering/workflow/#retrospective), each Engineering
team should be in the practice of holding their own retrospectives. The results
of these retrospectives should then inform the function-wide retrospective for
any given release. [More information on running effective retrospectives is
available here](/handbook/engineering/management/team-retrospectives/).

## Engineering Metrics

There are many ways to develop engineering metrics, ideally you need a representation of different aspects of the
 operational data such as throughput, quality and cycle time to name a few. 
 The goal of gathering this data is to foster a healthy dialogue amongst the engineering team and with stakeholders. 
 These metrics are helpful for conversations around planning, addressing technical debt, capacity to work on bugs,
identifying bottlenecks and improving engineering efficiency. It is important to note that if metrics are used 
punitively, these goals are hampered and the team psychological safety could be at risk.

Engineering metrics are project or team based and are not intended to track an individual's capacity or performance. 

To access our current dashboard, please visit the [GitLab Insight Quality Page](https://quality-dashboard.gitlap.com/groups/gitlab-org). 
We are currently working to implement these graphs into the GitLab product.

To read more about each metric, please visit their dedicated pages.

**List of Metrics**
  * [Throughput](/handbook/engineering/management/throughput/): This is our measure for number of MRs merged in a defined time frame (week and months). 
  The current calculations includes merges in all branches and not just `master`.
    * [Engineering throughput](https://quality-dashboard.gitlap.com/groups/gitlab-org/throughputs): 
    This chart shows the throughput at the `gitlab-org` group level.
    * [Group throughputs (using Plan:Portfolio Management as an example)](https://quality-dashboard.gitlap.com/groups/gitlab-org/sections/group::portfolio_management): 
    The throughput graph is also available for each group.
    * Data for throughput is available at the weekly and monthly level. We show 12 weeks for weekly and 24 months for monthly views.
  * [Average MRs merged per author per Month](https://quality-dashboard.gitlap.com/groups/gitlab-org/average_mrs_per_month): 
  This is our measure calculated by the total number of MRs merged in a given month divided by the total unique authors who created those MRs. 
  We currently get all the unique authors from that period and not just filtered to engineering.
  * **Deprecated** [Average MRs merged per engineer per Milestone](https://quality-dashboard.gitlap.com/groups/gitlab-org/average_mrs_per_milestone): 
  This is our measure calculated by the total number of MRs merged in a given milestone divided by the total unique engineers who created those MRs.
    * **Known issues** This chart is deprecated. Data at the milestone level has proven to be less accurate. 
    The reason is because of a lack of milestone hygiene due to missing or wrong milestones assigned to the MR.
    As a result we have moved to track data on a Time-Period basis via weeks/months. Please refer to the chart above instead.
  * [Bugs by Priority (monthly)](https://quality-dashboard.gitlap.com/groups/gitlab-org/bugs_by_priority): This chart shows the monthly bugs by priority over time.
  * [Bugs by Severity  (monthly)](https://quality-dashboard.gitlap.com/groups/gitlab-org/bugs_by_severity): This chart shows the monthly bugs by severity over time.
  * [Bug Classification](https://quality-dashboard.gitlap.com/groups/gitlab-org/bug_classification): This chart shows the overall of priority and severity of current open bugs.
  * [Regressions per Milestone](https://quality-dashboard.gitlap.com/groups/gitlab-org/regressions): This chart shows the number of regressions found in a release.
    * **Known issues** Data here is dependent on the `regression:x.y` label. It is possible that some regressions are not captured due to it not having a regression label.
  * [Weekly Pulse Survey](/handbook/engineering/management/pulse-survey/): We are working to roll out a weekly pulse survey across all of the engineering teams. 
  The goal is to provide a weekly set of data of how the individuals on the team feel in regards to 3 main areas: 
  Their work, their manager, and GitLab. 
  In addition to the numeric responses, a comment field is available. 
  This survey is anonymous and the comments in the results are only visible by the director administering the survey. 
### Data included in calculation

#### Engineering wide projects
We are currently working on including all of engineering's projects. The following projects included can be seen in this [list](https://quality-dashboard.gitlap.com/groups/gitlab-org/projects).

#### Security fixes on dev instance

We also capture data from our `https://dev.gitlab.org/` instance which currently includes the projects below. 

* gitlabhq (CE)
* gitlab-ee
* gitaly
* gitlab-shell
* gitlab-pages
* GitLab runner

The data from dev instance is only reflected in a few group level charts, see list of known issues below.

#### Known issues

* Due to the lack of team labels on `https://dev.gitlab.org/`, only the 3 group-level charts below are reflected with data from this source.
  * [Engineering throughput](https://quality-dashboard.gitlap.com/groups/gitlab-org/throughputs)
  * [Average MRs merged per engineer per Month](https://quality-dashboard.gitlap.com/groups/gitlab-org/average_mrs_per_month)
  * [Average MRs merged per engineer per Milestone](https://quality-dashboard.gitlap.com/groups/gitlab-org/average_mrs_per_milestone)
* Insufficient labelled data will be listed under `undefined` category.
* A small amount of Merge Requests are not visualized in the charts due to missing `merged_at` attribute. 
  * Bug filed https://gitlab.com/gitlab-org/gitlab-ce/issues/58061

### Adding more metrics

If you or your team needs to include a new project in the metrics or add a new team, please create an issue in the [GitLab Insights issue tracker](https://gitlab.com/gitlab-org/gitlab-insights/issues).

We have a list of projects that is still not included in this [issue](https://gitlab.com/gitlab-org/gitlab-insights/issues/56#projects-not-introduced-yet).
