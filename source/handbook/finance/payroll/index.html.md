---
layout: handbook-page-toc
title: "Payroll"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

Welcome to the Payroll Group handbook!  You should be able to find answers to most of your questions here.  If you't find what you are looking for then please do the following:
-  **Email** your question(s) to one of the following group email addresses:
   *   US team members: uspayroll@gitlab.com
   *   Non-US team members: nonuspayroll@gitlab.com
   *   Expenses: expenses@gitlab.com
-  **Chat channel**:  `#payroll_expenses` slack channel for questions that is not confidential.

## Legal Entity Payroll providers

- GitLab BV (Netherlands) - HR Savvy
- GitLab BV (Belgium) - B&F Consult Firm
- GitLab IT BV - iiPay payment service
- GitLab Federal (US) - ADP
- GitLab Inc (US) - ADP
- GitLab GmbH (Germany) - RPI
- Gitlab LTD (UK) - Vistra
- GitLab PTY (Australia) - iiPay
- GitLab Canada Corp (Canada) - TBD

## Pay Date

1. Employees of GitLab Inc and GitLab Federal will get pay on the 15th and the last day of the month.  The payroll schedule, pay slips, and W-2(s) are available on [ADP portal](workforcenow.adp.com)
1. Employees of GitLab BV (Netherlands) will get their salary wired on the 25th of every month, and can see their pay slip in their personal portal on
[HR Savvy's system](https://hr-savvy.nmbrs.nl/) towards the end of the month.
1. Employees of GitLab BV (Belgium) will get their salary wired the last day of each month and will receive their pay slip in their personal portal on [Boekfisk's system](http://www.boekfisk.be/)
1. Employees of GitLab Ltd will get their salary wired on the last day of every month, and can see their pay slip via their personal portal on [Vistra's system](https://www.vistra.com/) towards the end of the month.
1. Employees of GitLab GmbH will get their salary wired on the last day of every month, and can see their pay slip via their [person portal](https://www.datev.de/ano/) on [RPI International Payroll's system](http://roehm-international.com/) towards the end of the month.
1. Employees of GitLab PTY will get their salary on the 27th of each month, and their pay slip through [iiPay portal](https://pay.iipaysp.com/RegisterUser/GITL)
1. Employees of GitLab BV that are employed through GitLab's co-employer PE0:
   - [Lyra](http://lyrainfo.com/) (India) will get their salary wired around the last day of the month. Lyra will send pay slips electronically through their HR portal.
   - CXC (Canada) are paid bi-weekly. 
   - Safeguard are paid monthly and on the last day of the month (depending on the country) with exception to Brazil, 15th of each month and last day of the month. Payslips are provided electronically by Safeguard. 
1. Contractors from ( Nigeria, South Africa, US)  will get pay within 15 days after they submitted their monthly invoices for services to nonuspayroll@gitlab.com. 
1. Contractors from (Poland and Ukraine) will get pay around 27th or 28th of each month from CXC Global.
1. All other contractors will get pay on the 22nd of the month by iiPay and remittance advice will send to their gitlab email address.
 

## US

## Non-US

### Employees

### Contractors
- iiPay
- CXC Global

### PEO

#### SafeGuard
- Brazil
- France
- Hungary
- Ireland
- Italy
- Spain
- Switzerland

#### CXC Global
* Canada

#### Lyra

## Expenses

All team members will have access to Expensify within 2 days from their hire date.  If you didn't receive an email from Expensify for your access, then please contact expenses@gitlab.com. Expense reports are to be submitted once a month, at least. Additional information on getting started with Expensify and creating/submitting expense reports can be found [here.](https://docs.expensify.com/using-expensify-day-to-day/using-expensify-as-an-expense-submitter/report-actions-create-submit-and-close)

The procedure by which reimbursable expenses are processed varies and is dependent on contributor legal status (e.g. independent contractor, employee) and subsidiary assignment (Inc, LTD, BV, GmbH, PTY). Check with Payroll if you are unsure about either of these.

For information regarding the company expense policy, check out the section of our team handbook on [Spending Company Money](/handbook/spending-company-money). Managers and Payroll team will review the expenses for compliance with the company travel policy.  The CEO will review selected escalations at least annually.
Team members should also consider the terms and conditions of their respective contractor agreements, when submitting invoices to the company.

##### SafeGuard
Team members who are employed through SafeGuard must submit their expense for reimbursement through Expensify.  All expense reports must be submitted and approved by manager by the 8th of each month to include in the current month payment.
Team members in France, Italy, and Spain must submit their expenses through:
*  Expensify
*  Safeguard in-house expense reimbursement
*  GitLab payroll send the expense approval to Safeguard after the team member's manager approved the report
*  Team members send the original receipts to Safeguard

##### CXC
- Canada
- Poland and Ukraine

##### Lyra

* Team members must submit their expenses through Expensify, and Payroll will approve for reimbursement within 5 business days after the approval from the manager.  The reimbursement is through GitLab AP.

##### iiPay

##### Legal entities
* Expense reports for GitLab Ltd (UK) must be approved by the manager on or before the 14th of each month enable for it to include in the current month payroll.
* Expense reports for GitLab BV (Belgium and Netherlands), GmbH (Germany), PTY Ltd (Australia) are reimbursed via GitLab AP within 10 business days from the approval date by their manager.
* Expense reports for GitLab Inc, Gitlab Inc Billable, and GitLab Federal reimbursed via Expensify, and Payroll will final approved the report within 5 business days after the approval from their manager.

##### Non-Reimbursable Expenses
Examples of things we have not reimbursed:
1. Costume for end of summit party.
1. Boarding expense for dog while traveling.
1. Headphones costing $800 which were found to be in excess of our standard equipment guidelines.
1. Batteries for smoke detector.
1. Meals during the summit when team members opt out of the company provided meal option.
1. Cellphones and accessories.
1. Travel related expenses for family members of GitLab employees

In accordance with [Reimbursable Expense guidelines](/handbook/finance/accounting/#reimbursable-expenses), independent contractors should note which expenses are Contribute related on their invoices, prior to submitting to the company.

**Billable Expenses**
If you have an expense report that can be billed back to a customer please make sure to check the "billable" flag in Expensify along with tagging the customer name under the "customer" field in Expensify.
## Performance Indicators

### Payroll accuracy for each check date = 100%
Payroll is paid on time and accurately for each check date. 

### Payroll journal entry reports submitted to Accounting <= Payroll date + 2 business days
Payroll journal entry reports are to be submitted to Accounting no later than two business days after the payroll date. The payroll journal entry reports submitted dates are tracked in the Monthly Closing Checklist on a monthly basis. 
