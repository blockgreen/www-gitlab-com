---
layout: handbook-page-toc
title: "Field Marketing"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Field Marketing

The role of field marketing is to support marketing messages at a regional level through in-person interactions (quality vs. quantity) coupled with multi-touch activities. Field Marketing programs are focused on relationship building with customers and prospects to support land and expand opportunities as well as pulling pipeline through the funnel more quickly.

# Types of programs Field Marketing runs

## GitLab Owned Field Events

### [GitLab Connect](https://www.youtube.com/watch?v=aKwpNKoI4uU)
GitLab Connect is a full or half day event with both customers and prospects in attendance sharing stories & lessons learned about GitLab. SAL's will be responsible for asking customers to speak and Marketing, through a combination of SDR outreach, database and ad geotargeting will drive attendance to the event. If you would like to propose a GitLab Connect in your city, please open an issue in the [Marketing - Field Marketing project](https://gitlab.com/gitlab-com/marketing/field-marketing) using the [`AMER_Event_Request_Template`](https://gitlab.com/gitlab-com/marketing/field-marketing/issues/new?issuable_template=AMER_Event_Request_Template) for AMER events or the [`Field Event_GitLabConnect`](https://gitlab.com/gitlab-com/marketing/field-marketing/issues/new?issuable_template=Field%20Event_GitLabConnect) template for all other regions. Interested in seeing a GitLab Connect in action? [Check it out.](https://www.youtube.com/watch?v=aKwpNKoI4uU)

To see how we handle GitLab owned events from a Marketing Programs standpoint, please head over to [their handbook page](/handbook/marketing/marketing-sales-development/marketing-programs/#event-channel-types)

### GitLab run workshops
Field Marketers will work with Product Marketing & Technical Product Marketing to put together various types of workshops depending on the needs of the region. Workshops available to date can be found [here](https://gitlab.com/gitlab-workshops). 

### 3rd party events
We will sponsor regional 3rd party events in an effort to build the GitLab brand and also to gather leads. Different type of 3rd part events include, but are not limited to:

- DevOps Days
- Agile Events
- City run technology meetings
- Customer/prospect run DevOps events on invite 
- Executive relationship building events via companies like Apex Assembly & Argyle Executive Forum

To see how we handle GitLab owned events from a Marketing Programs standpoint, please head over to [their handbook page](/handbook/marketing/marketing-sales-development/marketing-programs/#event-channel-types) 

### Field Event Goals

- Sales Acceleration
   - Engaging with existing customers
   - New growth opportunities
- Demand
   - Education
- Market Intelligence
   - Test out new messaging or positioning

### Vendors who we work with in AMER 
We sometimes work with third party vendors for outreach, event production etc. Below is a list of whom we work with currently and the epic that tracks whom we have evaluated/worked with in the past.
* Emissary.io - in an effort to help sales gain account intelligence 
* Banzai - to supplement event recruiting 
* [FM vendor evaluation](https://gitlab.com/groups/gitlab-com/marketing/-/epics/441)

### Corporate Memberships owned by GitLab Field Marketing 

* [AFCEA](https://www.afcea.org/site/) - Membership is handled by the Public Sector Field Marketing Manager. Account information is stored in the marketing 1Pass vault. 
* [ACT-ICT](https://www.actiac.org) - Membership is handled by the Public Sector Field Marketing Manager. Please ping the PubSec FMM for details if you'd like to join. 
* [Technology Association of GA](https://www.tagonline.org/) - Membership is handled by the East FMM. If you'd like to take advantage of our membership, please email membership@tagonline.org from your GitLab account with the East FMM on copy. 

## What's currently scheduled in my region?

* Note to see the full list of events, you need to be sure you are logged into your GitLab account. There are times we make issues private.
- [AMER East](https://gitlab.com/groups/gitlab-com/marketing/-/boards/915674?&label_name[]=East) - Run by @GReib
- [AMER East-Central](https://gitlab.com/groups/gitlab-com/marketing/-/boards/1105137?&label_name[]=East%20-%20Central) - Run by @JSorensen
- [AMER West](https://gitlab.com/groups/gitlab-com/marketing/-/boards/933457?&label_name[]=West) - Run by @EmilyLuehrs
- [AMER Public Sector](https://gitlab.com/groups/gitlab-com/marketing/-/boards/933456?&label_name[]=Public%20Sector) - Run by @Hortel
- [APAC](https://gitlab.com/groups/gitlab-com/marketing/-/boards/933488?&label_name[]=APAC) - Run by @Phuynh
- [EMEA](https://gitlab.com/groups/gitlab-com/marketing/-/boards/933459?&label_name[]=EMEA) - Run by @amimmo

To find out what upcoming events GitLab will be involved in please visit our [Events Page](/events/). If you have any questions or an event suggestion please review [how to suggest an event at GitLab](/handbook/marketing/events/#suggesting-an-event)

For details on how we handle events & how to suggest an event for your region[(corporate or field, please check out this page)](/handbook/marketing/events/).

## Field Marketing Communications
The Field Marketing team use an email alias to manage communications related to field & owned events.   

The email `fieldmarketing@` company domain is used as an inbound communication channel **only** for messages related to attendance, diatary restrictions, etc.   

Emails sent *to* this address will create a **CASE** within Salesforce, assigning the record to the `Field Marketing` user profile. 

### How to Respond via Case 
##### [How to: Work with SFDC Cases](https://drive.google.com/open?id=1aEdCnLmzpcWsTKekdb5OG_smxzJIwskl) (*internal* only)

1. Navigate to the [Field Marketing Case view](https://gitlab.my.salesforce.com/500?fcf=00B4M000004O6Y4)^ within Salesforce. (^Must be signed in for the link to be active) 
     - View is filtered to show only **Open** cases. 
1. Click `Case Number` for the case to read detail.  
1. To send a response, scroll down to the `Emails` section
     - You will see a list of emails associated to this case (typically there is only one)
     - Far left side there is an `Action` column - **click** "Reply"
1. Change the `From` email to `"Field Marketing" <fieldmarketing@gitlab.com>`, you can add additional `CC:` or `BCC:` if needed. 
1. In the `Body` section, type your response as if you would in regular Gmail.  
1. Click `Send` when you are ready to send. 
1. Screen will update taking you back to the original **Case** screen. 
     - If the issue has be resolved, close the case. 
     - If still need to follow up any future responses will also be funneled into the same case. 

### Async Status Updates

Since we are a [remote](/company/culture/all-remote/) company, we utilize a Slack plugin called [Geekbot](https://geekbot.io/) to coordinate status updates. Field Marketing world wide currently conducts 1 weekly standup. Geekbot shares this update in the public #fieldmarketing slack room.

#### Weekly Status Update
The **Weekly Status Update** is configured to run at 10 AM local time on Monday, and contains 5 questions:

1. ***What was your favorite part of the weekend?*** 

    The goal with this question is for you to get to know your colleagues and for you to be able to share what excited you from the previous few days when you weren't working.

2. ***What's happening in your life?*** 

    Is there anything you'd like the team to know about what's going in your life? Feel free to share as much or as little as you feel comfortable sharing. 

3. ***Are you traveling anywhere this week? If so, where and why.*** 

    As Field Marketers, we travel up to 50% of the time. Sharing where you are is important, especially if are in a different timezone. 

4. ***What are your top 1-3 priorities for the next week?***

    These top 3 priorities should be focused on what you plan to accomplish that week. 

5. ***Anything blocking your progress?*** 

    Of those 1-3 items listed, do you need any roadblocks removed in order to accomplish the priorities? 
    
You will be notified via the Geekbot plug in on slack at 10 AM your local time on Mondays, as stated above. Its important to note, that unless you answer all 5 questoins in the Geekbot plug in, your answers will NOT be shared with your colleagues, so please be sure to complete all 5 questions! 

## Other pages to review for a full understanding of how Field Marketing at GitLab operates
* [Events at GitLab](/handbook/marketing/events/)
* [Marketing Program Management](/handbook/marketing/marketing-sales-development/marketing-programs)
* [Marketing Operations](/handbook/marketing/marketing-operations/)
* [Sales Development](/handbook/marketing/revenue-marketing/xdr/)
* [Links GitLab Field Marketers find useful](https://docs.google.com/spreadsheets/d/1gjJghF8Va-G0lYWsDaYXKG7JPtADLS2Jhrh8IVHkizQ/edit?ts=5d249a33#gid=1748424259&range=A1)
* [Field Marketing onboarding videos](https://drive.google.com/open?id=1m8ReMIiymMTqqk5PJAG7u_IG-Q5pkusV) - NOTE - these are also in the Field Marketing Onbaording issue that is kept in the [Marketing onboarding project](https://gitlab.com/gitlab-com/marketing/onboarding#onboarding)

