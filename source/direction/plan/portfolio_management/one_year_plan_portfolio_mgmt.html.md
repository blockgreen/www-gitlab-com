---
layout: markdown_page
title: "Group Vision - Strategic Portfolio Management"
---

- TOC
{:toc}

## 1 Year Plan

A Brief Sidenote: This is written in the format of an [internal press release](https://medium.com/bluesoft-labs/try-an-internal-press-release-before-starting-new-products-867703682934) describing the finished state of Strategic Portfolio Management functionality at the end of the corresponding period. It is subject to change based on feedback from internal stakeholders and the wider community. It's goal is to act as a compass as we continue to build and deliver value in an iterative and incremental manner.

### GitLab Launches Strategic Porfolio Management Capabilities

GitLab has built on top of their robust and flexible project management tools to bring you a Portfolio Management offering that enables you to keep multiple teams and projects aligned and executing on your business strategy.

#### Summary

30% of global software decision makers cite budgeting and 21% cite misalignment between IT and business
as the biggest challenges their firms face when orchestrating their software strategy. GitLab's new Portfolio Managment offerings enable the automated translation of enterprise strategic plans into product and project plans that drive business value and keep your initatives on track. Manage your entire portfolio of work at the executive and director level by mappping out your business initatives, track the projects and resources associated assigned to them, and get a clear view of dependencies and blockers.

#### Problem

At the Enterprise level, the complexities and difficulties of managing multiple teams and projects across your business portfolio are monumental. Not only do you need to have detailed information on individual team's progress against a project or initative, you need to be able to aggregate it up to a consumable view for executives and directors to review and make decisions against. This often requires you to purchase multiple software tools, integrate them together, and constantly ensure that data is being shared across each properly.

#### Solution

GitLab is now providing an array of new features and functionality to enable the enterprise business to manage their entire product decvelopment portfolio, all all within the industry leading unified DevOps platform!

**Mapping Effort to Strategic Value** : Create strategic initiatives and assign work to each, allowing you to track how much effort is being committed and make decisions on the allocation of effort to deliver more value to your consumers.

**Enhance Portfolio and Project Roadmaps**: Leverage easy to use and cross-team roadmaps at the portfolio, project, and epic level allowing users across the org to see how work is progressing and identify dependencies and blockers. Organize and prioritize work though dynamic roadmaps in real time by interacting with the roadmap.

**Easy Top Down Planning**: Enhanced Roadmaps and Value mapping allow you to start planning from the top, creating initiatives, projects, and epics and laying them out on a roadmap prior to issues and milestones being created.  

**Provide Portfolio and Project level Reporting and Analytics**: Review dashboarding and analytics, enabling your business to track and communicate progress on work in flight, capacity of teams and projects, and overall efficiency across their full portfolio.

## 3 Year Vision

The working format for this vision is a set of bullet points that represent major areas we ought to be really far along in solving by this point in time.

In 3 Years Portfolio Management will provide a robust and advanced set of functionality that enables businesses to manage their Product Development Portfolio with deep cross cutting Roadmapping, advanced Value Stream Mapping and Analytics, and intelligence driven Capacity and Release Management.

**Value Stream Mapping and Analytics**: Gain insight and make data driven decisions with advanced Value Stream analytics rolled up from all areas of your Portfolio.

**Intelligent Driven Capacity and Release Management**: Leverage in house intelligence to predict capacity, team output, and provide release date recommendations based on predicted effort, available capacity, and historical trends

**What-If Scenarios for Resource Management**: Run advanced theoretical scenarios on resource allocation and changes in capacity to help you decide where to make investments.
